 -- create database aeroport;
 
 
drop table if exists user;
drop table if exists flight;
drop table if exists city;



CREATE TABLE user (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `type` varchar(30) ,
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `iduser` (`iduser`),
  UNIQUE KEY `username` (`username`)
);


CREATE TABLE city (
  `id_city` int(11) NOT NULL AUTO_INCREMENT,
  `longitude` varchar(30) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL ,
  PRIMARY KEY (`id_city`),
  UNIQUE KEY `id_city` (`id_city`)
);

CREATE TABLE flight (
  `idflight` int(11) NOT NULL AUTO_INCREMENT,
  `airplane_type` varchar(30) ,
  `departure_city` int(30) ,
  `arrival_city` int(30)  ,
  `departure_time` varchar(30) ,
  `arrival_time` varchar(30) ,
  PRIMARY KEY (`idflight`),
  UNIQUE KEY `idflight` (`idflight`)
);

 alter table flight
	add constraint fk_flight_pornire
	foreign key (arrival_city)
	references city(id_city)
	on update cascade
	on delete cascade;
    
 alter table flight
	add constraint fk_flight_sosire
	foreign key (departure_city)
	references city(id_city)
	on update cascade
	on delete cascade;
    



INSERT INTO user (username, password,type) 
VALUES ('admin' , 'admin','admin') ,
 ('savan','catalin','normal');


INSERT INTO city (longitude, latitude,name) 
VALUES ('47.1416' , '23.8787','dej') ,
 ('46.7712','23.6236','cluj'),
 ('44.4268','26.1025','bucuresti'),
 ('47.4979','19.0402','budapeste'),
 ('47.1585','27.6014','iasi');