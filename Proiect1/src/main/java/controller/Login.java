package controller;

import model.User;
import service.UserService;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServler
 */
@WebServlet("/LoginProcessing")
public class Login extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        UserService userService = new UserService();
        User userDeValidat = userService.findByUsername(userName);

        if (userDeValidat!=null && userDeValidat.getPassword().equals(password) && session.getAttribute("user")==null)
        {
            if(userDeValidat.getType().equals("normal"))
            {

                request.setAttribute("username", userName);
                session.setAttribute("user", userDeValidat);
                request.getRequestDispatcher("/login/welcomeUser.jsp").forward(request, response);
            }
            if(userDeValidat.getType().equals("admin"))
            {
                request.setAttribute("username", userName);
                session.setAttribute("user", userDeValidat);
                request.getRequestDispatcher("/login/welcomeAdmin.jsp").forward(request, response);
            }
            else
            {

                    request.getRequestDispatcher("/login/login.jsp").forward(request, response);


            }

        } else {
            request.setAttribute("mesajEroare", "Cont Gresit sau inexistent");
           request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }
    }

}
