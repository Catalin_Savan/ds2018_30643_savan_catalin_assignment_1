package controller;

import model.User;
import service.CityService;
import service.FlightService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/AddFlight")
public class AddFlightController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddFlightController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

        if (session.getAttribute("user") != null && user.getType().equals("admin")) {

            CityService cityService = new CityService();


            request.setAttribute("citys", cityService.findCityes());
            request.getRequestDispatcher("flight/addFlight.jsp").forward(request, response);
        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

        if (session.getAttribute("user") != null && user.getType().equals("admin")) {

            FlightService flightService = new FlightService();

            String type = request.getParameter("typeAirplane");
            String orasPornire = request.getParameter("orasPornire");
            String orasSosire = request.getParameter("orasSosire");
            String timpDecolare = request.getParameter("timpDecolare");
            String timpAterizare = request.getParameter("timpAterizare");

            String dataDecolare = request.getParameter("dataDecolare");
            String dataAterizare = request.getParameter("dataAterizare");
            flightService.addFlight(type ,dataAterizare + " " + timpAterizare ,dataDecolare + " " + timpDecolare,orasSosire,orasPornire);
//            Flight newFlight = new Flight();
//
//
//            newFlight.setAirplane_type(type);
//            newFlight.setArrival_time(dataAterizare + " " + timpAterizare);
//            newFlight.setDeparture_time(dataDecolare + " " + timpDecolare);
//            newFlight.setArrival_city(orasPornire);
//            newFlight.setDeparture_city(orasSosire);
//
//            flightDao.save(newFlight);
            response.sendRedirect("/ViewFlights");
            //request.getRequestDispatcher("login/welcomeUser.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }


    }

}
