package controller;

import model.Flight;
import model.User;
import service.CityService;
import service.FlightService;
import service.TimeApiService;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/FindFlightsByCitys")
public class FindFlightsByCitysController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindFlightsByCitysController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

        if (session.getAttribute("user") != null && user.getType().equals("normal")) {

            CityService cityService = new CityService();

            request.setAttribute("citys", cityService.findCityes());
            request.getRequestDispatcher("flight/findByCitiesClient.jsp").forward(request, response);
        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

         CityService cityService = new CityService();
        if (session.getAttribute("user") != null && user.getType().equals("normal")) {


            FlightService flightService = new FlightService();

            TimeApiService timeApiService = new TimeApiService();

            String orasPornire = request.getParameter("orasPornire");
            String orasSosire = request.getParameter("orasSosire");

            String timp = timeApiService.findBycities(cityService.findByName(orasSosire).getLongitude(),cityService.findByName(orasSosire).getLatitude());

            request.setAttribute("time", timp );
            request.setAttribute("flights", flightService.findBycities(cityService.findByName(orasPornire),cityService.findByName(orasSosire)));
            request.getRequestDispatcher("flight/viewFlightClient.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }

    }

}
