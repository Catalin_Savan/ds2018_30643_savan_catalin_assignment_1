package controller;

import model.User;
import service.FlightService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/ViewFlights")
public class ViewFlightsController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewFlightsController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

        if (session.getAttribute("user") != null && user.getType().equals("admin")) {

            FlightService flightService = new FlightService();

            request.setAttribute("flights", flightService.getFlights());
            request.getRequestDispatcher("flight/viewFlight.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.

        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

}
