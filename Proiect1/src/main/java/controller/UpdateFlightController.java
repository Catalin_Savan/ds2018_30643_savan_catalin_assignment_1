package controller;

import model.User;
import service.CityService;
import service.FlightService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/updateFlight")
public class UpdateFlightController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateFlightController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

        if (session.getAttribute("user") != null && user.getType().equals("admin")) {

            CityService cityService = new CityService();
            request.setAttribute("citys", cityService.findCityes());
            request.getRequestDispatcher("flight/updateForm.jsp").forward(request, response);
        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");

        if (session.getAttribute("user") != null && user.getType().equals("admin")) {
            int id = Integer.parseInt(request.getParameter("flightId"));

            FlightService flightService = new FlightService();


            String type = request.getParameter("typeAirplane");
            String orasPornire = request.getParameter("orasPornire");
            String orasSosire = request.getParameter("orasSosire");
            String timpDecolare = request.getParameter("timpDecolare");
            String timpAterizare = request.getParameter("timpAterizare");

            String dataDecolare = request.getParameter("dataDecolare");
            String dataAterizare = request.getParameter("dataAterizare");


            flightService.updateFlight(id ,type ,dataAterizare + " " + timpAterizare ,dataDecolare + " " + timpDecolare,orasSosire,orasPornire);
            response.sendRedirect("/ViewFlights");
            //request.getRequestDispatcher("flight/viewFlight.jsp").forward(request, response);

        } else {
            request.setAttribute("mesajEroare", "Nu este permisa accesarea acestei pagini!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);//forwarding the request
        }


    }

}
