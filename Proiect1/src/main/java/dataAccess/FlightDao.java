package dataAccess;

import model.City;
import model.Flight;

import java.util.List;

public interface FlightDao {

    void delete(Flight flight);
    void deleteById(int entityId);
    void update(Flight flight);
    Flight findOne(int id);
    List<Flight> findAll();
    void save(Flight flight);
    List<Flight> findByCities(City arrival_time, City departure_city);
}
