package dataAccess.Implementation;

import dataAccess.FlightDao;
import model.City;
import model.Flight;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

public class FlightDaoImpl extends DaoGeneric<Flight> implements FlightDao {

    public FlightDaoImpl() {
        super(Flight.class);
    }


    public List<Flight> findByCities(City arrival_city, City departure_city) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Flight> query = cb.createQuery(Flight.class);
        ParameterExpression<City> idp = cb.parameter(City.class, "city");
        Root<Flight> flightRoot = query.from(Flight.class);
        query.select(flightRoot).where(cb.equal(flightRoot.get("arrival_city"), arrival_city), cb.equal(flightRoot.get("departure_city"), departure_city));
        TypedQuery<Flight> typedQuery = entityManager.createQuery(query);
        return typedQuery.getResultList();

    }




}