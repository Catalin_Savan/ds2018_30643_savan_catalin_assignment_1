package dataAccess.Implementation;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.List;

public abstract class DaoGeneric<T> {

    private Class<T> clazz;


    @PersistenceContext
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my-persistence-unit");

    EntityManager entityManager = emf.createEntityManager();


    public DaoGeneric(Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }


    public T findOne(int id) {
        return entityManager.find(clazz, id);
    }

    public List<T> findAll() {


        List<T> a =entityManager.createQuery("from " + clazz.getName())
                .getResultList();

        return a;
    }

    public void save(T entity) {

        System.out.println("AM AJUNS AICI !!! la save: " + entity.toString() + "\n");

        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
    }

    public void update(T entity) {
        System.out.println("AM AJUNS AICI !!! la update: " + entity.toString() + "\n");

        entityManager.getTransaction().begin();
        entityManager.merge(entity);
        entityManager.getTransaction().commit();
    }

    public void delete(T entity) {

        System.out.println("AM AJUNS AICI !!! la delete: " + entity.toString() + "\n");

        entityManager.getTransaction().begin();
        entityManager.remove(entity);
        entityManager.getTransaction().commit();
    }

    public void deleteById(int entityId) {
        T entity = findOne(entityId);
        ;
        delete(entity);
    }
}