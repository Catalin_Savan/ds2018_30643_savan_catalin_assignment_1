package dataAccess.Implementation;


import dataAccess.CityDao;
import model.City;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

public class CityDaoImpl extends DaoGeneric<City> implements CityDao {

    public CityDaoImpl() {
        super(City.class);
    }

    public City findByName(String oras) {


            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<City> query = cb.createQuery(City.class);
            ParameterExpression<String> OrasCautat = cb.parameter(String.class, "name");
            Root<City> cityRoot = query.from(City.class);
            query.select(cityRoot).orderBy(cb.asc(cityRoot.get("name")));
            query.where(cb.equal(cityRoot.get("name"), OrasCautat));
            TypedQuery<City> typedQuery = entityManager.createQuery(query);
            return typedQuery.setParameter("name", oras).getSingleResult();


    }

    public List<City> findAllCities() {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<City> query = cb.createQuery(City.class);
        Root<City> cityRoot = query.from(City.class);
        query.select(cityRoot).orderBy(cb.asc(cityRoot.get("name")));
        TypedQuery<City> typedQuery = entityManager.createQuery(query);
        return typedQuery.getResultList();

    }
}