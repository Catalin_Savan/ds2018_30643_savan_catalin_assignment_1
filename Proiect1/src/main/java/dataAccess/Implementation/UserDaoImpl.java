package dataAccess.Implementation;

import dataAccess.UserDao;
import model.User;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;


public class UserDaoImpl extends DaoGeneric<User> implements UserDao {





    public UserDaoImpl() {
        super(User.class);
    }




    public User findByUsername(String usernameCautat) {

        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<User> query = cb.createQuery(User.class);
            ParameterExpression<String> UsernameParametru = cb.parameter(String.class, "username");
            Root<User> userRoot = query.from(User.class);
            query.select(userRoot);
            query.where(cb.equal(userRoot.get("username"), UsernameParametru));
            TypedQuery<User> typedQuery = entityManager.createQuery(query);
            return typedQuery.setParameter("username", usernameCautat).getSingleResult();
        }
        catch (NoResultException e)
        {
            e.printStackTrace();

            return null;

        }

    }


}