package dataAccess;

import model.User;

import java.util.List;

public interface UserDao {
    void delete(User user);
    void deleteById(int entityId);
    void update(User user);

    User findByUsername(String usernameCautat);

    User findOne(int id);
    List<User> findAll();
    void save(User user);
}