package dataAccess;

import model.City;

import java.util.List;

public interface CityDao {
    void delete(City city);
    void deleteById(int entityId);
    void update(City city);
    City findOne(int id);
    List<City> findAll();
    void save(City city);
    City findByName(String oras);
    List<City> findAllCities();
}
