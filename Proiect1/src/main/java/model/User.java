package model;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;

@Entity
@OptimisticLocking(type = OptimisticLockType.ALL)
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "user")
public class User  {
    @Id
    @Column(name = "iduser", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private  String password;
    @Column(name = "type")
    private  String type;

    public User(int idUser,String username, String password , String type)
    {
        this.username = username;
        this.password = password;
        this.id=idUser;
        this.type=type;
    }

    public User(String username, String password , String type)
    {
        this.username = username;
        this.password = password;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public User(){}
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
