package model;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;

@Entity
@OptimisticLocking(type = OptimisticLockType.ALL)
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "flight")
public class Flight {

    //flight number, airplane type, departure
//city, departure date and hour, arrival city, arrival date and hour.
    @Id
    @Column(name = "idflight", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idflight;
    @Column(name = "airplane_type")
    private String airplane_type;
    @OneToOne(fetch=FetchType.LAZY,optional=false)
    @JoinColumn(name="arrival_city", referencedColumnName="id_city")
    private City arrival_city;
    @OneToOne(fetch=FetchType.LAZY,optional=false)
    @JoinColumn(name="departure_city", referencedColumnName="id_city")
    private City departure_city;
    @Column(name = "departure_time")
    private String departure_time;
    @Column(name = "arrival_time")
    private String arrival_time;

    public Flight() {
    }

    @Override
    public String toString() {
        return "Flight{" +
                "idflight=" + idflight +
                ", airplane_type='" + airplane_type + '\'' +
                ", departure_city='" + departure_city.toString() + '\'' +
                ", arrival_city='" + arrival_city.toString() + '\'' +
                ", departure_time='" + departure_time + '\'' +
                ", arrival_time='" + arrival_time + '\'' +
                '}';
    }

    public int getIdflight() {
        return idflight;
    }

    public void setIdflight(int idflight) {
        this.idflight = idflight;
    }

    public String getAirplane_type() {
        return airplane_type;
    }

    public void setAirplane_type(String airplane_type) {
        this.airplane_type = airplane_type;
    }

    public City getArrival_city() {
        return arrival_city;
    }

    public void setArrival_city(City arrival_city) {
        this.arrival_city = arrival_city;
    }

    public City getDeparture_city() {
        return departure_city;
    }

    public void setDeparture_city(City departure_city) {
        this.departure_city = departure_city;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }
}
