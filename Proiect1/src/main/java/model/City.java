package model;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;

@Entity
@OptimisticLocking(type = OptimisticLockType.ALL)
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "city")
public class City  {

    @Id
    @Column(name = "id_city", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_city;
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "latitude")
    private  String latitude;
    @Column(name = "name")
    private  String name;

    public City() {
    }

    @Override
    public String toString() {
        return "City{" +
                "id_city=" + id_city +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId_city() {
        return id_city;
    }

    public void setId_city(int id_city) {
        this.id_city = id_city;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
