package service;

import dataAccess.CityDao;
import dataAccess.Implementation.CityDaoImpl;
import model.City;

import java.util.List;

public class CityService {

    private CityDao cityDao = new CityDaoImpl();

    public List<City> findCityes()
    {
        return cityDao.findAllCities();
    }

    public City findByName(String name )
    {
        return cityDao.findByName(name);
    }
}
