package service;

import dataAccess.CityDao;
import dataAccess.Implementation.CityDaoImpl;
import dataAccess.FlightDao;
import dataAccess.Implementation.FlightDaoImpl;
import model.City;
import model.Flight;

import java.util.List;

public class FlightService {

    private FlightDao flightDao = new FlightDaoImpl();
    private CityDao cityDao = new CityDaoImpl();

    public List<Flight> getFlights() {
        return flightDao.findAll();
    }

    public void addFlight(String Airplane_type , String Arrival_time ,String Departure_time,String Arrival_city ,String Departure_city)
    {
        Flight newFlight = new Flight();



        newFlight.setAirplane_type(Airplane_type);
        newFlight.setArrival_time(Arrival_time);
        newFlight.setDeparture_time(Departure_time);
        newFlight.setArrival_city(cityDao.findByName(Arrival_city));
        newFlight.setDeparture_city(cityDao.findByName(Departure_city));

        flightDao.save(newFlight);
    }



    public void updateFlight( int id ,String Airplane_type , String Arrival_time ,String Departure_time,String Arrival_city ,String Departure_city)
    {
        Flight newFlight = flightDao.findOne(id);


        newFlight.setAirplane_type(Airplane_type);
        newFlight.setArrival_time(Arrival_time);
        newFlight.setDeparture_time(Departure_time);
        newFlight.setArrival_city(cityDao.findByName(Arrival_city));
        newFlight.setDeparture_city(cityDao.findByName(Departure_city));

        flightDao.update(newFlight);
    }


    public void deleteFlightById ( int id )
    {
        flightDao.deleteById(id);
    }

    public List<Flight> findBycities(City orasPornire , City orasDestinatie)
    {
       return flightDao.findByCities(orasDestinatie,orasPornire);
    }



}
