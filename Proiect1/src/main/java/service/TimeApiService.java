package service;

import org.apache.http.HttpEntity;



import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TimeApiService {


    public String findBycities(String longitude , String latitude ) {


        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("http://new.earthtools.org/timezone/"+longitude+"/"+latitude);

        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();

            InputStream stream = entity.getContent();
            StringBuffer content = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(content.toString())));
            NodeList errNodes = doc.getElementsByTagName("timezone");
            if (errNodes.getLength() > 0) {
                Element err = (Element) errNodes.item(0);
                System.out.println("localtime " + err.getElementsByTagName("localtime").item(0).getTextContent());

                return "localtime " + err.getElementsByTagName("localtime").item(0).getTextContent();
            }
        } catch (
                IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }


}
