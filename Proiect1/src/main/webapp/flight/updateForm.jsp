<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/login/welcomeAdmin.jsp"></jsp:include>



<div style="float: left" class="form">
    <form method="post" action="/updateFlight">
        <fieldset>
            <legend>Avion:</legend>
            <p>
                <label>Type Avion: <br>
                    <%--<input type="text" name="typeAirplane" required/>--%>
                    <select name="typeAirplane" required>
                        <option value="Air Europa" >Air Europa</option>
                        <option value="Business">Business</option>
                        <option value="Blue Air">Blue Air</option>
                        <option value="EuroWings" >EuroWings</option>
                        <option value="International Travel">International Travel</option>
                        <option value="National Travel">National Travel</option>
                        <option value="Pegasus Airlines">Pegasus Airlines</option>
                        <option value="Personal Plane">Personal Plane</option>
                        <option value="Swiss">Swiss</option>
                        <option value="Turkish Airlines">Turkish Airlines</option>
                        <option value="Wizz">Wizz</option>
                    </select>
                </label>
            </p>

            <p>
                <label>Oras pornire: </label>
                <select name="orasPornire" required>
                    <c:forEach items="${citys}" var="string">
                        <option value="${string.name}">
                                ${string.name}
                        </option>
                    </c:forEach>
                </select>
                <input type="time" name="timpDecolare" required/>
                <input type="date" name="dataDecolare" required/>
            </p>
            <p>
                <label>Oras sosire: </label>
                <select name="orasSosire" required>
                    <c:forEach items="${citys}" var="oras">
                        <option value="${oras.name}">
                                ${oras.name}
                        </option>
                    </c:forEach>
                </select>
                <input type="time" name="timpAterizare" required/>
                <input type="date" name="dataAterizare" required/>
            </p>
            <p>
                <input type="hidden" name="flightId" value="<%= request.getParameter("flightId")%>" required/>
            </p>

            <p>
                <input type="submit" value="Update">
            </p>
        </fieldset>
    </form>
</div>


</body>
</html>
