<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/login/welcomeAdmin.jsp"></jsp:include>


<table border="2" width="70%" cellpadding="2" class="table table-striped table-dark">
    <tr><th>Zbor ID </th><th>Type</th><th>Pornire</th><th>Sosire</th><th>OraPornire</th><th>OrsaSosire</th></tr>
    <tbody>
    <c:forEach var="flight" items="${flights}">
        <tr>

            <td>${flight.idflight}</td>

            <td>${flight.airplane_type}</td>

            <td>${flight.departure_city.name}</td>

            <td>${flight.arrival_city.name}</td>

            <td>${flight.departure_time}</td>

            <td>${flight.arrival_time}</td>

            <td>
                <form action="/updateFlight">
                    <input type="hidden" value="${flight.idflight}" name="flightId">
                    <button class="btn btn-primary" onclick="show()" type="submit" id="updateButton" name="update">update</button>
                </form>
            </td>

            <td>
                <form action="/deleteFlight"  method="get">
                    <input type="hidden" value="${flight.idflight}" name="flightId">
                    <button class="btn btn-primary" type="submit" id="delete" name="delete">Delete</button>
                </form>
            </td>

        </tr>
    </c:forEach>
    </tbody>
</table>




</body>
</html>
